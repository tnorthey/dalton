#!/bin/sh
#
# This is the script for generating files for a specific Dalton test job.
#
# For the .check file ksh or bash is preferred, otherwise use sh
# (and hope it is not the old Bourne shell, which will not work)
#
if [ -x /bin/ksh ]; then
   CHECK_SHELL='#!/bin/ksh'
elif [ -x /bin/bash ]; then
   CHECK_SHELL='#!/bin/bash'
else
   CHECK_SHELL='#!/bin/sh'
fi


#######################################################################
#  TEST DESCRIPTION
#######################################################################
cat > rsp_phosph_csf_error.info <<'%EOF%'
   rsp_phosph_csf_error
   ----------
   Molecule:         LiH
   Wave Function:    MCSCF (CAS) / Sadlej-pVTZ
   Test Purpose:     Phosphorescence
%EOF%

#######################################################################
#  MOLECULE INPUT
#######################################################################
cat > rsp_phosph_csf_error.mol <<'%EOF%'
BASIS
Sadlej-pVTZ
Calculation of phosphorescence

    2    2  X  Y    1 1.00D-12
        1.0   1
H           .0000000000             .0000000000            2.0969699107
        3.0   1
Li          .0000000000             .0000000000            -.9969699107
%EOF%

#######################################################################
#  DALTON INPUT
#######################################################################
cat > rsp_phosph_csf_error.dal <<'%EOF%'
**DALTON INPUT
.RUN RESPONS
**INTEGRALS
.SPIN-ORBIT
**WAVE FUNCTIONS
.HF
.MP2
.MCSCF
*SCF INPUT
.DOUBLY OCCUPIED
 2 0 0 0
*CONFIGURATION INPUT
.INACTIVE
 1 0 0 0
.ELECTRONS
 2
.CAS SPACE
 2 0 0 0
.SYMMET
 1
.SPIN MULT
 1
**RESPONS
*QUADRATIC
.PHOSPHORESENCE
.ROOTS
 1 1 0 0
**END OF DALTON INPUT
%EOF%
#######################################################################



#######################################################################

#######################################################################
#  CHECK SCRIPT
#######################################################################
echo $CHECK_SHELL >rsp_phosph_csf_error.check
cat >>rsp_phosph_csf_error.check <<'%EOF%'
log=$1

if [ `uname` = Linux ]; then
   GREP="egrep -a"
else
   GREP="egrep"
fi

if $GREP -q "not implemented for parallel calculations" $log; then
   echo "TEST ENDED AS EXPECTED"
   exit 0
fi

# Basis set
CRIT1=`$GREP "H * 1 * 1\.0000 * 18 * 9 * \[6s4p\|3s2p\]" $log | wc -l`
CRIT2=`$GREP "Li * 1 * 3\.0000 * 48 * 24 * \[10s6p4d\|5s3p2d\]" $log | wc -l`
CRIT3=`$GREP "total\: * 2 * 4\.0000 * 66 * 33" $log | wc -l`
CRIT4=`$GREP "Spherical harmonic basis used\." $log | wc -l`
TEST[1]=`expr	$CRIT1 \+ $CRIT2 \+ $CRIT3 \+ $CRIT4`
CTRL[1]=4
ERROR[1]="BASIS SET NOT READ CORRECTLY"

# Geometry
CRIT1=`$GREP "Total number of coordinates\: * 6" $log | wc -l`
CRIT2=`$GREP "1 * x * (0| )\.0000000000" $log | wc -l`
CRIT3=`$GREP "2 * y * (0| )\.0000000000" $log | wc -l`
CRIT4=`$GREP "3 * z * 2\.0969699107" $log | wc -l`
CRIT5=`$GREP "4 * x * (0| )\.0000000000" $log | wc -l`
CRIT6=`$GREP "5 * y * (0| )\.0000000000" $log | wc -l`
CRIT7=`$GREP "6 * z * (\-0|\-)\.9969699107" $log | wc -l`
TEST[2]=`expr	$CRIT1 \+ $CRIT2 \+ $CRIT3 \+ $CRIT4 \+ $CRIT5 \+ $CRIT6 \+ \
		$CRIT7`
CTRL[2]=7
ERROR[2]="GEOMETRY NOT READ CORRECTLY"

# Symmetry
CRIT1=`$GREP "Number of coordinates in each symmetry\: * 2 * 2 * 2 * 0" $log | wc -l`
CRIT2=`$GREP "Number of orbitals in each symmetry\: * 17 * 7 * 7 * 2" $log | wc -l`
TEST[3]=`expr	$CRIT1 \+ $CRIT2`
CTRL[3]=2
ERROR[3]="SYMMETRY NOT CORRECT"

# Energies
CRIT1=`$GREP "Hartree\-Fock total energy *\: * \-7\.98584206.." $log | wc -l`
CRIT2=`$GREP "\= MP2 second order energy *\: * \-8\.0233735..." $log | wc -l`
CRIT3=`$GREP "Final MCSCF energy\: * \-8\.0023102077.." $log | wc -l`
TEST[4]=`expr	$CRIT1 \+ $CRIT2 \+ $CRIT3`
CTRL[4]=3
ERROR[4]="ENERGIES NOT CORRECT"

# RESPONSE
CRIT1=`$GREP "@ Reason: A TRIPLET RESPONSE CALCULATION REQUIRES DETERMINANTS" $log| wc -l`
CTRL[5]=1
ERROR[5]="RESPONSE CALCULATION DOES NOT ABORT AS EXPECTED"

PASSED=1
for i in 1 2 3 4 5
do
   if [ ${TEST[i]} -ne ${CTRL[i]} ]; then
     echo "${ERROR[i]} ( test = ${TEST[i]}; control = ${CTRL[i]} ); "
     PASSED=0
   fi
done

if [ $PASSED -eq 1 ]
then
  echo TEST ENDED AS EXPECTED
  exit 0
else
  echo THERE IS A PROBLEM
  exit 1
fi

%EOF%
#######################################################################
